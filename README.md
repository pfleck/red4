# Redfour Space Travel
Elxir learning project. Explores problems related to space travel.

## Build Status
[![pipeline status](https://gitlab.com/pfleck/red4/badges/master/pipeline.svg)](https://gitlab.com/pfleck/red4/commits/master)

## Included Packages
* Physics -> Package for caculating physics problems found in rocketry.