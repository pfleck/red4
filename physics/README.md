# Physics
A Package designed to calculate various problems related to planetary physics and rocketry.

## Run tests
```Bash
> mix test

or 

> docker run -v "$PWD":/usr/src/physics -w /usr/src/physics elixir mix test
```

## Building & Running
```Bash
> mix escript.build
> ./physics
```