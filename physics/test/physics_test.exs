defmodule PhysicsRoketryTest do
    use ExUnit.Case
   
    test "escape velocity of earth is correct" do
        ev = Physics.Rocketry.escape_velocity(:earth)
        assert ev == 11.2
     end
    
    test "escape velocity of planet X is correct" do
        planet_x = %{mass: 4.0e22, radius: 6.21e6}
        ev =  planet_x |> Physics.Rocketry.escape_velocity
        assert ev == 0.9
    end
end

defmodule ConverterTest do
    use ExUnit.Case

    test "convert m to km" do
        assert 1 == (1000 |> Converter.convert_m_to_km)
        assert 1.337 == (1337 |> Converter.convert_m_to_km)
    end

    test "round to signification number" do
        assert 1.1 == (1.12 |> Converter.round_to_nearest_tenth)
        assert 1.2 == (1.18 |> Converter.round_to_nearest_tenth)
    end
end
