defmodule Converter do
    @moduledoc """
    Module that cotains converter function that a required for the rocketry module
    """

    def round_to_nearest_tenth(val) do
        Float.round val, 1
    end

    def convert_m_to_km(m) do
        m / 1000
    end

    # def to_light_seconds({:miles, miles} = val, precision: precision) do
    #     (miles * 5.36819e-6) |> round_up
    # end
    
    # def to_light_seconds({:meters, meters} = val, precision: precision)  do
    #     (meters * 3.335638620368e-9) |> round_up
    # end
    
    # def round_up(val) when is_float(val), do: trunc(val)
    
end


defmodule Physics.Rocketry do
    import Converter

    @doc """
    Calculates the escape velocity for our planet
    """
    def escape_velocity(:earth) do
        %{mass: 5.972e24, radius: 6.371e6}
          |> escape_velocity
    end
    
    @doc """
    Calculates the escape velocity for a give planet with mass and radius
    """
    def escape_velocity(planet) when is_map(planet) do
        planet
         |> calculate_escape_ms
         |> convert_m_to_km
         |> round_to_nearest_tenth
    end

    defp calculate_escape_ms(%{mass: mass, radius: radius}) do
        newtons_constant = 6.67e-11
        2 * newtons_constant * mass / radius |> :math.sqrt  # :math is erlangs math lib
    end

end