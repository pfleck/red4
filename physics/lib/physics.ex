defmodule Physics do
    def main(_ \\ []) do
        IO.puts "Earth Velocity:"
        IO.puts Physics.Rocketry.escape_velocity(:earth)
    end
end
